//
// Created by liu on 23-5-20.
//

#ifndef XLEX_PROGRAMSTRUCT_H
#define XLEX_PROGRAMSTRUCT_H

#include "ArithStruct.h"

namespace xLex::ProgramBlockStruct {
        enum meta {
            begin_block = xReChar('{'),
            end_block = xReChar('}'),
        };

        struct ProgramBlock;

        struct ProgramVariable {
            types::var_type type;
            xuInt name_len;
            char_t *name;
            ArithStruct::ArithExpression *val;
        };

        struct ControlStatement {
            enum {exp, blk} type;
            ArithStruct::ArithExpression * cond_expr;
            union {
              ArithStruct::ArithExpression * expr;
              struct ProgramBlock * block;
            } loop_body, else_part;
        };

        struct FinalStatement {
            xuInt n_members;
            ArithStruct::ArithExpression * members;
        };

        struct ProgramStatement {
            enum {
                control, final
            } type;
            union {
                ControlStatement * ctl;
                FinalStatement * final;
            } entry;
        };

        struct ProgramBlock {
            xuInt n_statement;
            ProgramStatement *statements;
        };
    }

#endif //XLEX_PROGRAMSTRUCT_H
