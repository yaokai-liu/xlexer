//
// Created by Yaokai Liu on 2023/4/14.
//

#ifndef XLEXER_STACK_H
#define XLEXER_STACK_H

#include "xtypes.h"
#include "xdef.h"
#include "Allocator.h"
#ifdef XLEXER_DEBUG
#include <cstdio>
#include <typeinfo>
#endif
namespace xLex {
    namespace _Stack {
        static xInt STACK_ALLOC_LEN = 4;
        static xInt STACK_COUNT = 0;
    }
    template<typename T>
    class Stack {
    private:
        xInt stack_id{};
        Allocator *allocator{};
        xLong buf_len{};
        xLong cur_len{};
        T *data{};
    public:
        explicit Stack(Allocator *allocator) :
                allocator(allocator),
                stack_id(_Stack::STACK_COUNT + 1) {
            _Stack::STACK_COUNT ++;
        };
        xVoid init();
        xVoid clear();
        xVoid push(T element);
        xVoid concat(T *ele_array, xLong len);
        xVoid concat(Stack<T> array);
        xVoid pop(T *dest);
        xLong len();
        T * realAddr(xLong index);
        T * realAddr(T * virtual_addr);
        T * virtualAddr(xLong index);
        T * virtualAddr(T * real_addr);

        const T & operator[](xLong index);
        const T & operator[](xLong index) const;
    };

    template<typename T>
    const T &Stack<T>::operator[](xLong index) const {
        return data[index];
    }

    template<typename T>
    const T &Stack<T>::operator[](xLong index) {
        return data[index];
    }

    template<typename T>
    T *Stack<T>::virtualAddr(T *real_addr) {
        xLong index = real_addr - data;
        return (T *) (
                (((xuLong) index) & 0x0000'0000'FFFF'FFFF)
                | (((xuLong)stack_id) << (sizeof(xuInt) * 8))
        );
    }

    template<typename T>
    T *Stack<T>::virtualAddr(xLong index) {
        return (T *) (
                (((xuLong) index) & 0x0000'0000'FFFF'FFFF)
                | (((xuLong)stack_id) << (sizeof(xuInt) * 8))
        );
    }

    template<typename T>
    T *Stack<T>::realAddr(T * virtual_addr) {
        if ((((xuLong)virtual_addr) >> (sizeof(xuInt) * 8)) == stack_id) {
            return &data[((xuLong)virtual_addr) & 0x0000'0000'FFFF'FFFF];
        }
        return nullptrof(T);
    }

    template<typename T>
    T *Stack<T>::realAddr(xLong index) {
        return &data[index];
    }

    template<typename T>
    xLong Stack<T>::len() {
        return cur_len;
    }

    template<typename T>
    xVoid Stack<T>::pop(T *dest) {
        cur_len--;
        if (dest) {
            allocator->memcpy(
                    static_cast<xVoid *>(dest),
                    static_cast<xVoid *>(&data[cur_len]),
                    sizeof(T));
        }
        allocator->memset(&data[cur_len], 0, sizeof(T));
    }

    template<typename T>
    xVoid Stack<T>::concat(T *ele_array, xLong len) {
        while (cur_len + len >= buf_len) {
            buf_len += _Stack::STACK_ALLOC_LEN;
            data = (T *) allocator->realloc(data, buf_len * sizeof(T));
        }
        allocator->memcpy(&data[cur_len], ele_array, len * sizeof(T));
        cur_len += len;
    }

    template<typename T>
    xVoid Stack<T>::concat(Stack<T> array) {
        return concat(array.data, array.cur_len);
    }

    template<typename T>
    xVoid Stack<T>::push(T element) {
        if (cur_len + 1 >= buf_len) {
#ifdef XLEXER_DEBUG
            fprintf(stdout, "allocate new memory for Stack<%s>\n", typeid(T).name());
#endif
            buf_len += _Stack::STACK_ALLOC_LEN;
            data = (T *) allocator->realloc(data, buf_len * sizeof(T));
        }
        allocator->memcpy(&data[cur_len], &element, sizeof(T));
        cur_len++;
    }

    template<typename T>
    xVoid Stack<T>::init() {
        buf_len = _Stack::STACK_ALLOC_LEN;
#ifdef XLEXER_DEBUG
        fprintf(stdout, "allocate new memory for Stack<%s>\n", typeid(T).name());
#endif
        data = (T *) allocator->malloc(buf_len * sizeof(T));
    }

    template<typename T>
    xVoid Stack<T>::clear() {
#ifdef XLEXER_DEBUG
        fprintf(stdout, "free memory for Stack<%s>\n", typeid(T).name());
#endif
        allocator->free(data);
        cur_len = 0;
        buf_len = 0;
    }

} // xLex

#endif //XLEXER_STACK_H
