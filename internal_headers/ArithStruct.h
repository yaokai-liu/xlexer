//
// Created by liu on 23-5-20.
//

#ifndef XLEX_ARITHSTRUCT_H
#define XLEX_ARITHSTRUCT_H

#include "xReChar.h"
#include "xLex_types.h"
#include "xLex_builtin.h"


namespace xLex::ArithStruct {
        enum meta : xuInt {
            // multi keywords
            assign = 1,
            equiv = 2,
            ge = 3,
            le = 4,

            begin_parentheses = xReChar('('),
            end_parentheses = xReChar(')'),
            last_val = xReChar('$'),
            grp_order = xReChar('#'),
            plus = xReChar('+'),
            minus = xReChar('-'),
            times = xReChar('*'),
            div = xReChar('/'),
            mod = xReChar('%'),
            concat = plus,
            gt = xReChar('>'),
            lt = xReChar('<'),
            eq = xReChar('='),
            l_and = xReChar('&'),
            l_not = xReChar('!'),
            l_or = xReChar('|'),
            colon = xReChar(':'),
            attr = xReChar('.'),
            comma = xReChar(','),
            s_quote = xReChar('\''),
            d_quote = xReChar('"'),
            semicolon = xReChar(';'),
            ques_mark = xReChar('?'),
        };

        extern const char_t * MULTI_WORD[];

        struct ArithExpression {
            enum e_type {
                fun, // function
                sin, // single operator
                bin, // binary operator
                num, // pure const_num
                var, // ProgramVariable defined in program
                data, // CapResult defined in regexp
            } e_type;
            enum types::var_type d_type;
            union {
                struct {
                    builtin::functor functor;
                    xuInt argc;
                    struct ArithExpression *argv;
                } function;
                struct {
                    builtin::functor functor;
                    struct ArithExpression *lhs;
                    struct ArithExpression *rhs;
                } bin_operator;
                struct {
                    builtin::functor functor;
                    struct ArithExpression *operand;
                } sin_operator;
                xuLong const_num;
                xVoid *rodata;
                struct ArithExpression *variable;
            } expr;
        };

        const static ArithStruct::ArithExpression BUILTIN_EXPRS[] = {
                {.e_type = ArithStruct::ArithExpression::num, .expr = {.const_num = 0}},
                {.e_type = ArithStruct::ArithExpression::num, .expr = {.const_num = 1}},
        };
        extern const char_t * const ASSIGN;
    }

#endif //XLEX_ARITHSTRUCT_H
