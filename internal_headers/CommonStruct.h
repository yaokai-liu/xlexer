//
// Created by liu on 23-5-20.
//

#ifndef XLEX_COMMONSTRUCT_H
#define XLEX_COMMONSTRUCT_H

#include "xReChar.h"

using namespace xre;

namespace xLex::CommonStruct {
        extern const char_t * const WHITESPACE;
    }

#endif //XLEX_COMMONSTRUCT_H
