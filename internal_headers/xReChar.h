//
// Created by Yaokai Liu on 2023/4/24.
//

#ifndef XRE_XRECHAR_H
#define XRE_XRECHAR_H

#include "xtypes.h"

namespace xre {

    typedef xByte ascii;

#ifdef XRE_CHAR_USING_CHAR
    typedef char   char_t;
#define xReChar(x)     x
#elif defined(XRE_CHAR_USING_WCHAR)
#ifdef __WCHAR_TYPE__
    typedef __WCHAR_TYPE__  char_t;
#else
    typedef xInt     char_t;
#endif
#define xReChar(x)      L##x
#define xReString(x)    L##x
#elif defined(XRE_CHAR_USING_ASCII)
typedef ascii char_t;
#define xReChar(x) x
#define xReString(x)    ((xre::char_t *) x)
#elif defined(XRE_CHAR_USING_XASCII)
    typedef xAscii char_t;
#define xReChar(x)      x
#define xReString(x)    ((xAscii *) (x))
#elif defined(XRE_CHAR_USING_XCHAR)
    typedef xChar char_t;
#define xReChar(x)      ((xChar) x)
#define xReString(x)    ((xChar *) x)
#else
#error "You should specify character type."
#endif

#ifndef XRE_CHAR_USING_XCHAR

    xuInt xRe_ord(char_t _the_char);

    char_t xRe_chr(xuInt _the_order);

#else
#define xRe_ord     ord
#define xRe_chr     chr
#endif

}

#endif //XRE_XRECHAR_H
