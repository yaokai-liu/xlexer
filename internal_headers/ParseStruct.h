//
// Created by liu on 23-5-20.
//

#ifndef XLEX_PARSESTRUCT_H
#define XLEX_PARSESTRUCT_H

#include "ArithStruct.h"

namespace xLex::ParseExpStruct {
        enum meta : char_t {
            escape = xReChar('\\'),

            begin_group = xReChar('('),
            end_group = xReChar(')'),
            begin_class = xReChar('['),
            end_class = xReChar(']'),
            begin_quantifier = xReChar('{'),
            end_quantifier = xReChar('}'),
            begin_label = xReChar('<'),
            end_label = xReChar('>'),
            at_start = xReChar('~'),
            at_end = xReChar('&'),

            end_plain = xReChar(' '),
            range_to = xReChar('-'),
            colon_hint = xReChar(':'),

            comma = xReChar(','),
            split = xReChar('|'),

            assign = xReChar('='),
            refer_call = xReChar('@'),
            last_val = xReChar('$'),

            any_plain = xReChar('.'),
            posure = xReChar('+'),
            closure = xReChar('*'),
            ques_mark = xReChar('?'),

            only_match = xReChar('!'),
            inverse = xReChar('^')
        };

        typedef char_t Range[2];

        enum escape_type {
            non_escape, class_escape, plain_escape
        };

        struct ReObj {
            xBool is_inverse;
            xBool only_match;
        };

        struct Sequence : public ReObj {
            xuInt n_plains;
            char_t *plains;
        };

        struct Class : public ReObj {
            xuInt n_plains;
            xuInt n_ranges;
            xuInt n_inv_plains;
            xuInt n_inv_ranges;
            char_t *plains;
            Range *ranges;
            char_t *inv_plains;
            Range *inv_ranges;
        };

        struct Quantifier : public ReObj {
            const ArithStruct::ArithExpression *min;
            const ArithStruct::ArithExpression *max;
            const ArithStruct::ArithExpression *step;
            ReObj *obj;
        };

        struct Branch {
            xuInt n_objects;
            ReObj **objects;
        };

        struct CapResult;

        struct Group : public ReObj {
            xuInt n_branches;
            Branch *branches;
            CapResult *cap;
        };

        struct Refer : public ReObj {
            enum refer_type {
                call, val
            } type;
            CapResult *cap;
        };

        struct CapResult {
            ReObj *obj;
            xuInt cap_times;
            xuInt last_len;
            char_t *last_val;
        };

        struct Label {
            char_t * name;
            xuInt name_len;
            CapResult *cap;
        };

        extern const char_t * const NON_PLAIN;
        extern const char_t * const SINGLE_ESCAPE;
        extern const char_t * const CLASS_ESCAPE;
        extern const struct Class CLASS_TEMP[4];
    }

#endif //XLEX_PARSESTRUCT_H
