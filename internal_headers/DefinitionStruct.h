//
// Created by liu on 23-7-9.
//

#ifndef XLEX_DEFINITIONSTRUCT_H
#define XLEX_DEFINITIONSTRUCT_H

#include "xLex_String.h"
#include "xLex_types.h"

namespace xLex::DefinitionStruct {
    extern const char_t * KW_TOKEN;
    extern const char_t * KW_VOID;

    typedef struct AttrItem {
        char_t * name;
        xLong name_len;
        types::var_type type;
        AttrItem * children;
        xuInt n_children;
        xuInt size;
    } AttrItem;

    typedef struct Token {
        char_t * name;
        xLong name_len;
        AttrItem * attrs;
        xuInt n_attrs;
    } Token;

}

#endif //XLEX_DEFINITIONSTRUCT_H
