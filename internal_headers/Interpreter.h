//
// Created by Yaokai Liu on 2023/4/13.
//

#ifndef XLEXER_INTERRUPTER_H
#define XLEXER_INTERRUPTER_H

#include "xLex_Stack.hpp"
#include "ArithStruct.h"
#include "ParseStruct.h"
#include "ProgramStruct.h"
#include "DefinitionStruct.h"

#ifdef XLEXER_DEBUG
#include <cstdio>
#endif

namespace xLex {
    class Interpreter {
        typedef enum xLex::ArithStruct::ArithExpression::e_type ident_type;
    private:
        char_t *cur_text{};
        char_t *cur_rule{};
        char_t *err_pos{};
        xLong err_len{};
        Allocator *allocator{};

        char_t * cur_line_start{};
        xLong cur_row{};
        xLong cur_col{};
        char_t err_msg[64]{};
        xLong msg_len{};
        const static char_t * error_message[50];

        Stack<char_t> plain_array{allocator};
        Stack<char_t> inv_plain_array{allocator};
        Stack<ParseExpStruct::Range> range_array{allocator};
        Stack<ParseExpStruct::Range> inv_range_array{allocator};

        Stack<ArithStruct::ArithExpression> expr_array{allocator};

        Stack<ParseExpStruct::Sequence> seq_array{allocator};
        Stack<ParseExpStruct::Class> class_array{allocator};
        Stack<ParseExpStruct::Quantifier> quantifier_array{allocator};
        Stack<ParseExpStruct::Branch> branch_array{allocator};
        Stack<ParseExpStruct::Group> group_array{allocator};
        Stack<ParseExpStruct::CapResult> capture_array{allocator};
        Stack<ParseExpStruct::Label> label_array{allocator};
        Stack<ParseExpStruct::Refer> refer_array{allocator};
        Stack<ParseExpStruct::ReObj *> obj_array{allocator};

        Stack<ProgramBlockStruct::ProgramVariable> program_var_array{allocator};
        Stack<ProgramBlockStruct::ProgramStatement> program_stat_array{allocator};
        Stack<ProgramBlockStruct::ProgramBlock> program_block_array{allocator};
        Stack<DefinitionStruct::Token> token_definition_array{allocator};
        Stack<DefinitionStruct::AttrItem> attr_tree_array{allocator};

        typedef struct {
            char_t * name;
            xLong name_len;
            DefinitionStruct::Token * token_type;
            ParseExpStruct::Group *parser;
            ProgramBlockStruct::ProgramBlock *program;
        } Rule;
        Stack<Rule> rule_array{allocator};

        [[maybe_unused]]
        xInt testEscape(char_t *regexp);

        // used
        xLong passWhitespace(char_t *regexp);

        xLong catchIdentifier(char_t *regexp);

        xLong testAssignment(char_t *src);

        xLong parsePlains(char_t *regexp);

        xLong parseClass(char_t *regexp);

        xLong parseValLiteral(char_t *regexp, xLong *index, xBool *is_group);

        xLong createLabel(char_t *regexp);

        xLong parseSingleEscape(char_t *regexp);

        xLong catchBinOp(char_t *src, builtin::functor *val_dest);

        xLong getRuleIndex(char_t * src, xLong length);

        xLong getLabelIndex(char_t * src, xLong length);

        xLong getVarIndex(char_t * src, xLong length);

        static xLong getBuiltinIndex(char_t * src, xLong length);

        xLong getTokenDefinitionIndex(char_t * src, xLong length);

        xLong getIdentTypeAndIndex(char_t * src, xLong length, ident_type * type, xLong * index);

        xLong getAttrItemIndex(DefinitionStruct::AttrItem item, Stack<DefinitionStruct::AttrItem>item_array);

        xLong compileEscape(char_t *regexp, xBool *is_class);

        xLong compileArithExpression(char_t *src);

        xLong compileSingleExpr(char_t *src);

        xLong compileGetCapData(xLex::char_t *src);

        xLong compileArgList(char_t *src, xLong *argc);

        xLong compileBinOpRHS(char_t *src, xInt bin_op);

        xLong compileGroup(char_t *regexp, xLong *grp_index);

        xLong compileSequence(char_t *regexp);

        xLong compileClass(char_t *regexp);

        xLong compileQuantifier(char_t *regexp);

        xLong compileQuantifierMacro(char_t *regexp);

        xLong compileAssignLabel(char_t *regexp);

        xLong compileCallLabel(char_t *regexp);

        xLong compileProgramBlock(char_t *src);

        xLong compileStatement(char_t *src, xBool *is_final);

        xLong compileControlStatement(char_t *src);

        xLong compileFinalStatement(char_t *src);

        xLong compileAssignment(char_t *src);

        xLong compileTokenDefinition(char_t *src);

        xLong compileDefinitionItem(char_t *src);

        xLong compileItemList(char_t *src, xLong *n_items);

        xLong compileRule(char_t *src);

        xVoid posToRowCol(xLong *err_row, xLong *err_col);

#ifdef XLEXER_DEBUG
        FILE *trace_out = stdout;
        Stack<char_t> trace_indent{allocator};
#endif

        xVoid staticDataInit();
    public:
        explicit Interpreter(Allocator *allocator);

        ~Interpreter();

        xLong compile(char_t *src);

        [[nodiscard]]
        xBool hasError() const;

        xLong logError(char_t *log_dest);
    };
}

#define check(expr, except_len) do { \
    if (!(expr)) { \
        err_pos = sp; \
        err_len = except_len; \
        return 0; \
    } \
} while (0)

#define eat_keyword(kw, except_len) do { \
    xBool ___ = ((except_len) == 1) \
              ? ((xuLong)(*sp) != (xuLong)kw) \
              : (strcmp_o(xReString(kw), sp) != (except_len)); \
    if (___) { \
        char_t *_fmt = ((except_len) == 1) \
                     ? xReString("\"%c\" is expected here.") \
                     : xReString("\"%s\" is expected here."); \
        strfmt_dsc(err_msg, _fmt, kw); \
        err_pos = sp; \
        err_len = (except_len); \
        msg_len = (except_len) + 17; \
        return 0; \
    } \
    sp += except_len; \
} while (false)


#ifdef XLEXER_DEBUG
#define trackEnter() do { \
    fprintf(trace_out, "%sEnter <%s>, sp = %ld;\n", &trace_indent[0], __FUNCTION__, sp - cur_text); \
    trace_indent.push(xReChar('-')); \
} while(0)

#define trackLeave() do { \
    trace_indent.pop(nullptr); \
    fprintf(trace_out, "%sLeave <%s>, sp = %ld;\n", &trace_indent[0], __FUNCTION__, sp - cur_text); \
} while(0)
#else
#define trackEnter() do {0;} while(0)
#define trackLeave() do {0;} while(0)
#endif

#endif //XLEXER_INTERRUPTER_H
