//
// Created by Yaokai Liu on 2023/4/14.
//

#ifndef XLEXER_STRING_H
#define XLEXER_STRING_H

#include "xReChar.h"

namespace xLex {
    using namespace xre;

    xInt stridx(const char_t *_string, char_t _chr);

    xInt strcmp_o(const char_t *_str1, const char_t *_str2);

    xBool strcmp_i(const char_t *_str1, const char_t *_str2, xuLong len);

    // String to long, while digits all are unsigned decimal.
    xInt str2l_ud(const char_t *_string, xLong *val_dest);

    // long to string
    xInt l2str_d(xLong val, char_t *dest_string);

    // string copy
    xInt strcpy(char_t * _dest, const char_t * _src);

    // format | only support for decimal, string and char.
    xInt strfmt_dsc(char_t * _dest, char_t * _format, ...);
} // xLex

#endif //XLEXER_STRING_H
