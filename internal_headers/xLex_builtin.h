//
// Created by Yaokai Liu on 2023/4/28.
//

#ifndef XLEXER_BUILTIN_H
#define XLEXER_BUILTIN_H

#include "xReChar.h"
#include "xLex_types.h"

namespace xLex {
    using namespace xre;
    namespace builtin {
        enum functor {
            nop = 0,
            // binary operators
            // name consists: [i,s]_<operator>_[i,s]
            // in <operand> <operator> <operand> form
            i_add_i,    // '+'
            i_sub_i,    // '-'
            i_mul_i,    // '*'
            i_div_i,    // '/'
            i_mod_i,    // '%'
            s_concat_s, // '+'
            i_repeat_s, // '*'
            s_repeat_i = i_repeat_s, // '*'
            i_gt_i,     // '>'
            i_lt_i,     // '<'
            i_eq_i,     // '=='
            b_and_b,    // '&'
            b_or_b,     // '|'

            // single operators
            get_cap_val,    // '$'
            get_cap_time,   // '#'
            not_i,          // '!'

            // single arg functions
            to_upper_s,
            to_lower_s,

            invalid = 255,
        };
        constexpr xuInt N_BUILTIN = 18;
        constexpr xuInt SYM_LEN = 8;


        extern const xVoid *BUILTIN[N_BUILTIN];
        extern const enum types::var_type BUILTIN_D_TYPE[N_BUILTIN];
        extern const char_t SYMBOLS[N_BUILTIN][SYM_LEN];

        extern const xuInt  PRIORITIES[];
    }
}

#endif //XLEXER_BUILTIN_H
