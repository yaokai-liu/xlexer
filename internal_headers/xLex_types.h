//
// Created by liu on 23-6-15.
//

#ifndef XLEX_TYPES_H
#define XLEX_TYPES_H
#include "xLex_String.h"

namespace xLex::types {
    enum var_type {
        d_char, d_int16, d_int32, d_int64, d_string, d_bool, d_struct, d_union
    };
    extern const char_t * KW_CHAR;
    extern const char_t * KW_INT16;
    extern const char_t * KW_INT32;
    extern const char_t * KW_INT64;
    extern const char_t * KW_STRING;
    extern const char_t * KW_BOOL;
    extern const char_t * KW_STRUCT;
    extern const char_t * KW_UNION;
}
#endif //XLEX_TYPES_H
