//
// Created by Yaokai Liu on 2023/4/14.
//

#ifndef XLEXER_ALLOCATOR_H
#define XLEXER_ALLOCATOR_H

namespace xLex {
    struct Allocator {
        xVoid *(*malloc)(xSize size);

        xVoid *(*realloc)(xVoid *ptr, xSize size);

        xVoid *(*calloc)(xLong count, xSize size);

        xVoid (*free)(xVoid *mem);

        xVoid (*memcpy)(xVoid *dest, xVoid *src, xSize size);

        xVoid (*memset)(xVoid *mem, xuByte value, xSize size);
    };
} // xLex

#endif //XLEXER_ALLOCATOR_H
