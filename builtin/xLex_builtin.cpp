//
// Created by Yaokai Liu on 2023/4/28.
//

#include "xLex_builtin.h"

namespace xLex::builtin {
    const xuInt  PRIORITIES[] = {
            [nop] = 0,
            [i_add_i] = 10,
            [i_sub_i] = 10,
            [i_mul_i] = 15,
            [i_div_i] = 15,
            [i_mod_i] = 15,
            [s_concat_s] = 10,
            [i_repeat_s] = 15,
    };
    const xVoid *BUILTIN[N_BUILTIN] = {};
    const enum types::var_type BUILTIN_D_TYPE[N_BUILTIN] = {};
    const char_t SYMBOLS[N_BUILTIN][SYM_LEN] = {};
}