//
// Created by liu on 23-7-22.
//

#include "DefinitionStruct.h"
namespace xLex::types {
    const char_t * KW_CHAR = xReString("char");
    const char_t * KW_INT16 = xReString("int16");
    const char_t * KW_INT32 = xReString("int32");
    const char_t * KW_INT64 = xReString("int64");
    const char_t * KW_STRING = xReString("string");
    const char_t * KW_BOOL = xReString("bool");
    const char_t * KW_STRUCT = xReString("struct");
    const char_t * KW_UNION = xReString("union");
}

namespace xLex::DefinitionStruct {
    const char_t * KW_TOKEN = xReString("token");
    const char_t * KW_VOID = xReString("void");
}
