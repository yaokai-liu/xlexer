//
// Created by liu on 23-5-20.
//
#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compileBinOpRHS(char_t *src, xInt bin_op) {
    char_t *sp = src;
    trackEnter();

    ArithStruct::ArithExpression exp = {};

    sp += compileSingleExpr(sp);
    if (err_len > 0) return 0;

    xLong length = {};
    do {
        auto *LHS = expr_array.virtualAddr(expr_array.len() - 1);
        builtin::functor bin_op2 = builtin::functor::invalid;
        sp += passWhitespace(sp);
        length = catchBinOp(sp, &bin_op2);
        // If RHS is a single expression,
        // or the followed operator's priority is lower,
        // return it.
        if (length == 0 || builtin::PRIORITIES[bin_op] >= builtin::PRIORITIES[bin_op2]) break;
        // invalid operator will cause a syntax error.
        check(bin_op2 == builtin::functor::invalid, length);
        sp += length;

        // Otherwise parse RHS of {bin_op2}.
        sp += compileBinOpRHS(sp, bin_op2);
        if (err_len > 0) return 0;
        auto *RHS = expr_array.virtualAddr(expr_array.len() - 1);

        // construct the expression
        ArithStruct::ArithExpression _exp = {};
        _exp.e_type = ArithStruct::ArithExpression::bin;
        _exp.expr.bin_operator.functor = bin_op2;
        _exp.expr.bin_operator.lhs = LHS;
        _exp.expr.bin_operator.rhs = RHS;
        expr_array.push(_exp);
    } while (true);

    trackLeave();
    return sp - src;
}
