//
// Created by liu on 23-5-20.
//

#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compileSingleExpr(char_t *src) {
    char_t *sp = src;
    trackEnter();

    sp += passWhitespace(sp);

    // parse parentheses expression
    if (*sp == ArithStruct::begin_parentheses) {
        sp++;
        xLong length = compileArithExpression(sp);
        check(length > 0, 1); sp += length;
        eat_keyword(ArithStruct::end_parentheses, 1);
        trackLeave();
        return sp - src;
    }

    ArithStruct::ArithExpression exp = {};

    // if is logic not
    if (*sp == ArithStruct::l_not) {
        sp++;
        xLong length = compileArithExpression(sp);
        check(length > 0, 1);
        sp += length;
        exp.e_type = ArithStruct::ArithExpression::sin;
        exp.d_type = types::d_bool;
        exp.expr.sin_operator.functor = builtin::not_i;
        exp.expr.sin_operator.operand = expr_array.virtualAddr(expr_array.len() - 1);
        expr_array.push(exp);
        trackLeave();
        return sp - src;
    }

    // if is last value or cap-times of capture result.
    if (*sp == ArithStruct::last_val
     || *sp == ArithStruct::grp_order) {
        sp += compileGetCapData(sp);
        if (err_len > 0) return 0;
        trackLeave();
        return sp - src;
    }

    // if it is a const_num
    xLong val = 0;
    xLong length = str2l_ud(sp, &val);
    if(length > 0) {
        sp += length;
        exp.e_type = ArithStruct::ArithExpression::num;
        exp.d_type = types::d_int32;
        exp.expr.const_num = val;
        expr_array.push(exp);
        trackLeave();
        return sp - src;
    }

    // otherwise is identifier
    length = catchIdentifier(sp);
    check(length > 0, 1);
    xLong index = -1;
    sp += getIdentTypeAndIndex(sp, length, &exp.e_type, &index);
    if (err_len > 0) return 0;

    if (exp.e_type == ArithStruct::ArithExpression::fun) {
        xLong argc = 0;

        sp += passWhitespace(sp);
        sp += compileArgList(sp, &argc);
        if (err_len > 0) return 0;

        exp.expr.function.argc = argc;
        exp.expr.function.argv = expr_array.virtualAddr(expr_array.len() - argc);
        exp.expr.function.functor = static_cast<builtin::functor>(index + 1);
        exp.d_type = builtin::BUILTIN_D_TYPE[index + 1];
        expr_array.push(exp);

        trackLeave();
        return sp - src;
    }

    if (exp.e_type == ArithStruct::ArithExpression::var) {
        exp.expr.variable = program_var_array[index].val;
        expr_array.push(exp);
        trackLeave();
        return sp - src;
    }

    if (exp.e_type == ArithStruct::ArithExpression::data) {
#define arg exp // reuse exp as the arg, for saving stack space.
        // Data must be accessed by special functions,
        // and data as argument.
        arg.e_type = ArithStruct::ArithExpression::data;
        arg.expr.rodata = label_array[index].cap;
        expr_array.push(arg);
#undef arg

        allocator->memset(&exp, 0, sizeof(exp));
        exp.e_type = ArithStruct::ArithExpression::sin;
        exp.expr.sin_operator.functor = builtin::get_cap_time;
        exp.expr.sin_operator.operand = expr_array.virtualAddr(expr_array.len() - 1);
        expr_array.push(exp);

        trackLeave();
        return sp - src;
    }

    err_pos = sp;
    err_len = length;
    return 0;
}
