//
// Created by liu on 23-5-20.
//

#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compileGetCapData(xLex::char_t *src) {
    ArithStruct::ArithExpression exp = {};
    ArithStruct::ArithExpression arg = {};
    char_t *sp = src;
    trackEnter();

    check(*sp == ArithStruct::last_val || *sp == ArithStruct::grp_order, 1);

    exp.e_type = ArithStruct::ArithExpression::sin;
    exp.expr.sin_operator.functor = (*sp == ArithStruct::last_val)
                                  ? builtin::functor::get_cap_val
                                  : builtin::functor::get_cap_time;
    exp.d_type = (*sp == ArithStruct::last_val) ? types::d_string : types::d_int32;
    sp++;
    sp += passWhitespace(sp);

    arg.e_type = ArithStruct::ArithExpression::data;
    xLong length = catchIdentifier(sp);
    xLong index = -1;
    if (length == 0) {
        // means it is a const_num, means cap-result of group
        length = str2l_ud(sp, &index);
        check(length > 0, 1);
        check(index < group_array.len(), length);
        arg.expr.rodata = group_array[index].cap;
    } else if ((index = getLabelIndex(sp, length)) >= 0) {
        // means it is a label, cap-result of label
        arg.expr.rodata = label_array[index].cap;
    } else {
        goto __failed_get_data;
    }
    expr_array.push(arg);

    exp.expr.sin_operator.operand = expr_array.virtualAddr(expr_array.len() - 1);
    expr_array.push(exp);
    sp += length;
    trackLeave();
    return sp - src;

    __failed_get_data:
    err_pos = sp;
    err_len = length;
    return 0;
}

