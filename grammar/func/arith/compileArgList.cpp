//
// Created by liu on 23-5-20.
//
#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compileArgList(xLex::char_t *src, xLong *argc) {
    Stack<ArithStruct::ArithExpression> argv_array{allocator};
    ArithStruct::ArithExpression exp{};

    char_t *sp = src;
    trackEnter();
    *argc = 0;
    eat_keyword(ArithStruct::begin_parentheses, 1);
    sp += passWhitespace(sp);
    if (*sp == ArithStruct::end_parentheses) {
        sp++;
        trackLeave();
        return sp - src;
    }

    argv_array.init(); // using a temporary array to store arguments
    __parse_args:
    sp += compileArithExpression(sp);
    if (err_len > 0) return 0;
    expr_array.pop(&exp);
    argv_array.push(exp);
    sp += passWhitespace(sp);

    if (*sp == ArithStruct::comma) {
        sp++;
        goto __parse_args;
    }

    eat_keyword(ArithStruct::end_parentheses, 1);

    // concat arguments to expr_array
    // now arguments are in a continuous memory.
    expr_array.concat(argv_array);
    *argc = argv_array.len();
    argv_array.clear();

    trackLeave();
    return sp - src;
}
