//
// Created by liu on 23-5-20.
//

#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compileArithExpression(char_t *src) {
    char_t *sp = src;
    trackEnter();

    sp += compileSingleExpr(sp);
    if (err_len > 0) return 0;

    xLong length = {};
    do {
        auto *LHS = expr_array.virtualAddr(expr_array.len() - 1);
        builtin::functor bin_op = builtin::functor::invalid;
        sp += passWhitespace(sp);
        length = catchBinOp(sp, &bin_op);
        // If length is zero, means no more bin_op,
        // means this expression is completed. Break loop.
        if (length == 0) break;
        // Otherwise there is a bin_op.
        // Invalid operator will cause a syntax error.
        check(bin_op != builtin::functor::invalid, length);
        sp += length;
        // start parse RHS
        sp += compileBinOpRHS(sp, bin_op);
        if (err_len > 0) return 0;
        sp += passWhitespace(sp);
        auto *RHS = expr_array.virtualAddr(expr_array.len() - 1);

        // construct the expression
        ArithStruct::ArithExpression _exp = {};
        _exp.e_type = ArithStruct::ArithExpression::bin;
        _exp.expr.bin_operator.functor = bin_op;
        _exp.expr.bin_operator.lhs = LHS;
        _exp.expr.bin_operator.rhs = RHS;
        expr_array.push(_exp);
    } while (true);
    trackLeave();
    return sp - src;
}
