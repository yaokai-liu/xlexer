//
// Created by liu on 23-7-11.
//

#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::getAttrItemIndex(DefinitionStruct::AttrItem item, Stack<DefinitionStruct::AttrItem>item_array) {
    for (xuInt i = 0; i < item_array.len(); i++) {
        xBool _ = item_array[i].name_len == item.name_len;
        auto * name1 = plain_array.realAddr(item.name);
        auto * name2 = plain_array.realAddr(item_array[i].name);
        _ = _ && strcmp_i(name1, name2, item.name_len);
        if (_) return i;
    }
    return -1;
}

xLong xLex::Interpreter::compileItemList(char_t *src, xLong *n_items) {
    Stack<DefinitionStruct::AttrItem> item_array{allocator};
    char_t * sp = src;
    trackEnter();

    eat_keyword(ProgramBlockStruct::begin_block, 1);
    sp += passWhitespace(sp);

    item_array.init();
    DefinitionStruct::AttrItem item = {};
    while ((*sp) != ProgramBlockStruct::end_block) {
        char_t *tp = sp;
        sp += compileDefinitionItem(sp);
        if (err_len > 0) return 0;
        attr_tree_array.pop(&item);
        if (getAttrItemIndex(item, item_array) >= 0) {
            err_pos = tp;
            err_len = sp - tp;
            return 0;
        }
        item_array.push(item);
        sp += passWhitespace(sp);
    }

    check(item_array.len() > 0, 1);
    eat_keyword(ProgramBlockStruct::end_block, 1);

    attr_tree_array.concat(item_array);
    *n_items = item_array.len();

    item_array.clear();

    trackLeave();
    return sp - src;

};
