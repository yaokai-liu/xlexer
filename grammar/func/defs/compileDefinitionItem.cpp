//
// Created by liu on 23-7-11.
//

#include "xLex_String.h"
#include "Interpreter.h"

using namespace xLex::types;
xLong xLex::Interpreter::compileDefinitionItem(char_t *src) {
    char_t * sp = src;
    trackEnter();

    DefinitionStruct::AttrItem item = {};

    if (4 == strcmp_o(sp, KW_CHAR)) {
        item.type = d_char;
        item.size = 1;
        item.children = nullptr;
        item.n_children = 0;
        sp += 4;
    } else if (5 == strcmp_o(sp, KW_INT16)) {
        item.type = d_int16;
        item.size = 2;
        item.children = nullptr;
        item.n_children = 0;
        sp += 5;
    } else if (5 == strcmp_o(sp, KW_INT32)) {
        item.type = d_int32;
        item.size = 4;
        item.children = nullptr;
        item.n_children = 0;
        sp += 5;
    } else if (5 == strcmp_o(sp, KW_INT64)) {
        item.type = d_int64;
        item.size = 8;
        item.children = nullptr;
        item.n_children = 0;
        sp += 5;
    } else if (6 == strcmp_o(sp, KW_STRING)) {
        item.type = d_string;
        item.size = 16;
        item.children = nullptr;
        item.n_children = 0;
        sp += 6;
    } else if (4 == strcmp_o(sp, KW_BOOL)) {
        item.type = d_bool;
        item.size = 1;
        item.children = nullptr;
        item.n_children = 0;
        sp += 4;
    } else if (6 == strcmp_o(sp, KW_STRUCT)) {
        item.type = d_struct;
        item.size = 0;
        sp += 6;
        // parse item list block
        sp += passWhitespace(sp);
        xLong n_items = 0;
        sp += compileItemList(sp, &n_items);
        if (err_len > 0) return 0;
        item.children = attr_tree_array.virtualAddr(attr_tree_array.len() - n_items);
        item.n_children = n_items;
    } else if (5 == strcmp_o(sp, KW_UNION)) {
        item.type = d_union;
        item.size = 0;
        sp += 5;
        // parse item list block
        sp += passWhitespace(sp);
        xLong n_items = 0;
        sp += compileItemList(sp, &n_items);
        if (err_len > 0) return 0;
        item.children = attr_tree_array.virtualAddr(attr_tree_array.len() - n_items);
        item.n_children = n_items;
    } else {
        err_pos = sp;
        err_len = 1;
        return 0;
    }

    sp += passWhitespace(sp);
    xLong length = catchIdentifier(sp);

    item.name = plain_array.virtualAddr(plain_array.len());
    plain_array.concat(sp, length);
    item.name_len = length;
    sp += length;

    eat_keyword(ArithStruct::semicolon, 1);

    attr_tree_array.push(item);
    trackLeave();
    return sp - src;
}
