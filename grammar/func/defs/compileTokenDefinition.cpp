//
// Created by liu on 23-7-9.
//

#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compileTokenDefinition(char_t * src) {
    char_t * sp = src;
    trackEnter();

    DefinitionStruct::Token token_def = {};

    eat_keyword(DefinitionStruct::KW_TOKEN, 5);
    sp += passWhitespace(sp);

    xLong n_items = 0;
    sp += compileItemList(sp, &n_items);
    if (err_len > 0) return 0;

    token_def.attrs = attr_tree_array.virtualAddr(attr_tree_array.len() - n_items);
    token_def.n_attrs = n_items;

    sp += passWhitespace(sp);

    xLong length = catchIdentifier(sp);
    check(length > 0, 1);
    if (getTokenDefinitionIndex(src, length) >= 0) {
        err_pos = sp;
        err_len = length;
        return -1;
    }

    token_def.name = plain_array.virtualAddr(plain_array.len());
    plain_array.concat(sp, length);
    token_def.name_len = length;
    sp += length;

    sp += passWhitespace(sp);
    eat_keyword(ArithStruct::semicolon, 1);

    token_definition_array.push(token_def);

    trackLeave();
    return sp - src;
}