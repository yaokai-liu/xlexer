//
// Created by liu on 23-5-20.
//
#include "Interpreter.h"

xLong xLex::Interpreter::compileClass(char_t *regexp) {
    char_t *sp = regexp;
    trackEnter();

    xLong plains = plain_array.len();
    xLong ranges = range_array.len();
    xLong inv_plains = inv_plain_array.len();
    xLong inv_ranges = inv_range_array.len();

    sp += parseClass(sp);

    ParseExpStruct::Class cls = {};
    cls.plains = plain_array.virtualAddr(plains);
    cls.n_plains = plain_array.len() - plains;
    cls.ranges = range_array.virtualAddr(ranges);
    cls.n_ranges = range_array.len() - ranges;
    cls.inv_plains = inv_plain_array.virtualAddr(inv_plains);
    cls.n_inv_plains = inv_plain_array.len() - inv_plains;
    cls.inv_ranges = inv_range_array.virtualAddr(inv_ranges);
    cls.n_inv_ranges = inv_range_array.len() - inv_ranges;

    class_array.push(cls);

    trackLeave();
    return sp - regexp;
}
