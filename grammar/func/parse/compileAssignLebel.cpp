//
// Created by liu on 23-5-20.
//

#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compileAssignLabel(char_t *regexp) {
    char_t *sp = regexp;
    trackEnter();

    eat_keyword(ParseExpStruct::assign, 1);

    eat_keyword(ParseExpStruct::begin_label, 1);

    sp += passWhitespace(sp);

    sp += createLabel(sp);
    if (err_len > 0) return 0;

    sp += passWhitespace(sp);

    eat_keyword(ParseExpStruct::end_label, 1);

    auto *label = &label_array[label_array.len() - 1];
    ParseExpStruct::Refer ref = {};
    ref.cap = label->cap;
    ref.type = ParseExpStruct::Refer::call;
    refer_array.push(ref);

    trackLeave();
    return sp - regexp;
}
