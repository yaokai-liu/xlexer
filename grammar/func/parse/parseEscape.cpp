//
// Created by liu on 23-5-20.
//

#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compileEscape(char_t *regexp, xBool *is_class) {
    char_t *sp = regexp;
    trackEnter();

    eat_keyword(ParseExpStruct::escape, 1);

    xLong index = stridx(ParseExpStruct::CLASS_ESCAPE, *sp);
    if (index >= 0) {
        *is_class = true;
        sp++;
        // {idx} determinate which TEMPLATE uses, and
        // {inv} determinate which kind of arrays receives.
        xuInt idx = index / 2, inv = index % 2;
        ParseExpStruct::Class cls0 = {};
        auto *cls = &ParseExpStruct::CLASS_TEMP[idx];
        inv ? (cls0.inv_plains = inv_plain_array.virtualAddr(inv_plain_array.len()))
            : (cls0.plains = plain_array.virtualAddr(plain_array.len()));
        inv ? (cls0.inv_ranges = inv_range_array.virtualAddr(inv_range_array.len()))
            : (cls0.ranges = range_array.virtualAddr(range_array.len()));
        auto *tp = inv ? &inv_plain_array : &plain_array;
        auto *tr = inv ? &inv_range_array : &range_array;
        tp->concat(cls->plains, cls->n_plains);
        tr->concat(cls->ranges, cls->n_ranges);
        inv ? (cls0.n_inv_plains = cls->n_plains) : (cls0.n_plains = cls->n_plains);
        inv ? (cls0.n_inv_ranges = cls->n_ranges) : (cls0.n_ranges = cls->n_ranges);
        class_array.push(cls0);
        trackLeave();
        return sp - regexp;
    }
    *is_class = false;
    return compileSequence(regexp);
}
