//
// Created by liu on 23-5-20.
//

#include "Interpreter.h"

xLong xLex::Interpreter::compileSequence(char_t *regexp) {
    char_t *sp = regexp;
    trackEnter();

    xLong start = plain_array.len();

    __parse_plains:
    sp += parsePlains(sp);
    if (err_len > 0) return 0;
    if (*sp == ParseExpStruct::escape) {
        xLong offset = parseSingleEscape(sp);
        if (offset > 0) goto __parse_plains;
    }

    check(sp > regexp, 1);

    xuInt n_plains = plain_array.len() - start;
    ParseExpStruct::Sequence seq = {};
    seq.n_plains = n_plains;
    seq.plains = plain_array.virtualAddr(start);
    seq_array.push(seq);

    trackLeave();
    return sp - regexp;
}
