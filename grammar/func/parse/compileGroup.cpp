//
// Created by liu on 23-5-20.
//
#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compileGroup(char_t *regexp, xLong *grp_index) {
    char_t *sp = regexp;
    trackEnter();

    eat_keyword(ParseExpStruct::begin_group, 1);

    group_array.push({});
    *grp_index = group_array.len() - 1;

    xuInt n_branches = branch_array.len();
    auto *branches = branch_array.realAddr(branch_array.len());

    ParseExpStruct::Branch branch = {};
    branch.n_objects = obj_array.len();
    branch.objects = obj_array.virtualAddr(obj_array.len());

    xBool is_inverse = false, only_match = false;
    while (*sp && *sp != ParseExpStruct::end_group) {
        sp += passWhitespace(sp);
        switch (*sp) {
            case ParseExpStruct::inverse: {
                is_inverse = true;
                sp++;
                continue;
            }
            case ParseExpStruct::only_match: {
                only_match = true;
                sp++;
                continue;
            }
            case ParseExpStruct::escape: {
                xBool is_class = {};
                sp += compileEscape(sp, &is_class);
                if (err_len > 0) return 0;
                is_class ? obj_array.push(class_array.virtualAddr(class_array.len() - 1))
                         : obj_array.push(seq_array.virtualAddr(seq_array.len() - 1));
                break;
            }
            case ParseExpStruct::begin_class: {
                sp += compileClass(sp);
                if (err_len > 0) return 0;
                obj_array.push(class_array.virtualAddr(class_array.len() - 1));
                break;
            }
            case ParseExpStruct::begin_group: {
                xLong index = -1;
                sp += compileGroup(sp, &index);
                if (err_len > 0) return 0;
                obj_array.push(group_array.virtualAddr(index));
                break;
            }
            case ParseExpStruct::begin_quantifier: {
                sp += compileQuantifier(sp);
                if (err_len > 0) return 0;
                auto *quan = quantifier_array.realAddr(quantifier_array.len() - 1);
                quan->obj = obj_array[obj_array.len() - 1];
                *(obj_array.realAddr(obj_array.len() - 1)) = quantifier_array.virtualAddr(quan);
                break;
            }
            case ParseExpStruct::assign: {
                sp += compileAssignLabel(sp);
                if (err_len > 0) return 0;
                auto *refer = refer_array.realAddr(refer_array.len() - 1);
                auto * cap = capture_array.realAddr(refer->cap);
                cap->obj = *(obj_array.realAddr(obj_array.len() - 1));
                obj_array.push(refer_array.virtualAddr(refer));
                break;
            }
            case ParseExpStruct::last_val:
            case ParseExpStruct::refer_call: {
                sp += compileCallLabel(sp);
                if (err_len > 0) return 0;
                obj_array.push(refer_array.virtualAddr(refer_array.len() - 1));
                break;
            }
            case ParseExpStruct::posure:
            case ParseExpStruct::closure:
            case ParseExpStruct::ques_mark: {
                sp += compileQuantifierMacro(sp);
                if (err_len > 0) return 0;
                break;
            }
            case ParseExpStruct::split: {
                branch.n_objects = obj_array.len() - branch.n_objects;
                branch_array.push(branch);
                branch.n_objects = 0;
                branch.objects = obj_array.virtualAddr(obj_array.len());
                sp ++; break;
            }
            default: {
                sp += compileSequence(sp);
                if (err_len > 0) return 0;
                obj_array.push(seq_array.virtualAddr(seq_array.len() - 1));
            }
        }
        is_inverse = false, only_match = false;
    }

    eat_keyword(ParseExpStruct::end_group, 1);

    ParseExpStruct::Group *grp = group_array.realAddr(*grp_index);
    grp->n_branches = branch_array.len() - n_branches;
    grp->branches = branches;
    ParseExpStruct::CapResult cap = {};
    cap.obj = group_array.virtualAddr(grp);
    capture_array.push(cap);
    grp->cap = capture_array.virtualAddr(capture_array.len() - 1);

    trackLeave();
    return sp - regexp;
}
