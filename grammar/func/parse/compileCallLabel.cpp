//
// Created by liu on 23-5-20.
//
#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compileCallLabel(char_t *regexp) {
    char_t *sp = regexp;
    trackEnter();

    ParseExpStruct::Refer::refer_type type;
    if (*sp == ParseExpStruct::refer_call) {
        type = ParseExpStruct::Refer::call;
        sp++;
    } else if (*sp == ParseExpStruct::last_val) {
        type = ParseExpStruct::Refer::val;
        sp++;
    } else {
        err_pos = sp;
        err_len = 1;
        return 0;
    }

    eat_keyword(ParseExpStruct::begin_label, 1);

    sp += passWhitespace(sp);

    xLong index = 0;
    xBool is_group;
    sp += parseValLiteral(sp, &index, &is_group);
    if (err_len > 0) return 0;

    sp += passWhitespace(sp);

    eat_keyword(ParseExpStruct::end_label, 1);

    ParseExpStruct::Refer ref = {};
    ref.type = type;
    ref.cap = is_group
              ? group_array[index].cap
              : label_array[index].cap;

    refer_array.push(ref);

    trackLeave();
    return sp - regexp;
}
