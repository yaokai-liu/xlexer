//
// Created by liu on 23-5-20.
//

#include "Interpreter.h"

xLong xLex::Interpreter::compileQuantifierMacro(char_t *regexp) {
    char_t *sp = regexp;
    trackEnter();

    ParseExpStruct::Quantifier quan = {};
    quan.step =  &ArithStruct::BUILTIN_EXPRS[1];
    switch (*sp) {
        case ParseExpStruct::posure: {
            quan.min =  &ArithStruct::BUILTIN_EXPRS[1];
            quan.max =  &ArithStruct::BUILTIN_EXPRS[0];
            sp ++; break;
        }
        case ParseExpStruct::closure: {
            quan.min =  &ArithStruct::BUILTIN_EXPRS[0];
            quan.max =  &ArithStruct::BUILTIN_EXPRS[0];
            sp ++; break;
        }
        case ParseExpStruct::ques_mark: {
            quan.min =  &ArithStruct::BUILTIN_EXPRS[0];
            quan.max =  &ArithStruct::BUILTIN_EXPRS[1];
            sp ++; break;
        }
        default: {
            err_pos = sp;
            err_len = 1;
            return 0;
        }
    }
    quantifier_array.push(quan);

    trackLeave();
    return sp - regexp;
}
