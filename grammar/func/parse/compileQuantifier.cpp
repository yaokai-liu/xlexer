//
// Created by liu on 23-5-20.
//

#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compileQuantifier(char_t *regexp) {
    char_t *sp = regexp;
    trackEnter();

    eat_keyword(ParseExpStruct::begin_quantifier, 1);

    ParseExpStruct::Quantifier quan = {};

    // min
    sp += passWhitespace(sp);
    if (*sp == ArithStruct::comma) {
        quan.min = expr_array.virtualAddr(0L);
        eat_keyword(ArithStruct::comma, 1);
    } else if (*sp == ArithStruct::semicolon) {
        eat_keyword(ArithStruct::semicolon, 1);
        quan.min = expr_array.virtualAddr(0L);
        quan.max = quan.min;
        goto __compose_step;
    } else if (*sp == ParseExpStruct::end_quantifier) {
        eat_keyword(ParseExpStruct::end_quantifier, 1);
        goto __compose_default_min;
    } else {
        sp += compileArithExpression(sp);
        if (err_len > 0) return 0;
        quan.min = expr_array.virtualAddr(expr_array.len() - 1);
        if (*sp == ArithStruct::comma) {
            eat_keyword(ParseExpStruct::comma, 1);
        } else if (*sp == ArithStruct::semicolon) {
            eat_keyword(ArithStruct::semicolon, 1);
            quan.max = quan.min;
            goto __compose_step;
        }
    }

    // max
    sp += passWhitespace(sp);
    if (*sp == ArithStruct::semicolon) {
        quan.max = quan.max ? quan.max : expr_array.virtualAddr(0L);
        eat_keyword(ArithStruct::semicolon, 1);
    } else if (*sp == ParseExpStruct::end_quantifier) {
        eat_keyword(ParseExpStruct::end_quantifier, 1);
        goto __compose_default_max;
    } else {
        sp += compileArithExpression(sp);
        if (err_len > 0) return 0;
        quan.max = expr_array.virtualAddr(expr_array.len() - 1);
        if (*sp == ArithStruct::semicolon) {
            eat_keyword(ArithStruct::semicolon, 1);
        }
    }

    // step
    __compose_step:
    sp += passWhitespace(sp);
    if (*sp == ParseExpStruct::end_quantifier) {
        eat_keyword(ParseExpStruct::end_quantifier, 1);
        goto __compose_default_step;
    } else {
        sp += compileArithExpression(sp);
        if (err_len > 0) return 0;
        quan.step = expr_array.virtualAddr(expr_array.len() - 1);
        sp += passWhitespace(sp);
        eat_keyword(ParseExpStruct::end_quantifier, 1);
        goto __compose_quan;
    }

    __compose_default_min:
    quan.min = expr_array.virtualAddr(0L);
    __compose_default_max:
    quan.max = expr_array.virtualAddr(0L);
    __compose_default_step:
    quan.step = expr_array.virtualAddr(1L);

    __compose_quan:
    quantifier_array.push(quan);

    trackLeave();
    return sp - regexp;
}

