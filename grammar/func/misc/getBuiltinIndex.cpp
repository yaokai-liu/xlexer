//
// Created by liu on 23-5-21.
//

#include "xLex_builtin.h"
#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::getBuiltinIndex(char_t * src, xLong length) {
    for (xuInt i = 0; i < builtin::N_BUILTIN; i++) {
        if (strcmp_i(src, builtin::SYMBOLS[i], length)) {
            return i;
        }
    }
    return -1;
}
