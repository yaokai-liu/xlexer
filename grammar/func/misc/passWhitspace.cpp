//
// Created by liu on 23-5-20.
//
#include "xLex_String.h"
#include "Interpreter.h"
#include "CommonStruct.h"

xLong xLex::Interpreter::passWhitespace(char_t *regexp) {
    char_t *sp = regexp;
    trackEnter();

    while (stridx(CommonStruct::WHITESPACE, *sp) >= 0) {
        if (stridx(xReString("\n\v"), *sp) >= 0) {
            cur_line_start = sp + 1;
            cur_row ++;
        }
        sp++;
    }

    trackLeave();
    return sp - regexp;
}
