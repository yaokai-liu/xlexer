//
// Created by liu on 23-5-20.
//

#include "xLex_String.h"
#include "ArithStruct.h"
#include "Interpreter.h"
using namespace xre;

xLong xLex::Interpreter::getIdentTypeAndIndex(char_t * src, xLong length, ident_type * type, xLong * index) {

    *index = getBuiltinIndex(src, length);
    if (*index >= 0) {
        *type = ArithStruct::ArithExpression::fun;
        return length;
    }

    *index = getVarIndex(src, length);
    if (*index >= 0) {
        *type = ArithStruct::ArithExpression::var;
        return length;
    }


    *index = getLabelIndex(src, length);
    if (*index >= 0) {
        *type = ArithStruct::ArithExpression::data;
        return length;
    }

    err_pos = src;
    err_len = length;
    return 0;
}
