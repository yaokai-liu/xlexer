//
// Created by liu on 23-5-21.
//

#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::getLabelIndex(char_t * src, xLong length) {
    for (xuInt i = 0; i < label_array.len(); i++) {
        xBool _ = (length == label_array[i].name_len);
        auto * name = plain_array.realAddr(label_array[i].name);
        _ = _ && strcmp_i(src, name, length);
        if (_) return i;
    }
    return -1;
}
