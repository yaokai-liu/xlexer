//
// Created by liu on 23-5-20.
//
#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::createLabel(char_t *regexp) {
    char_t *sp = regexp;
    trackEnter();

    xLong length = catchIdentifier(sp);
    if (length == 0) {
        err_pos = sp;
        err_len = 1;
        return 0;
    }
    check(getLabelIndex(sp, length) == -1, length);

    ParseExpStruct::Label label = {};

    label.name = plain_array.virtualAddr(plain_array.len());
    plain_array.concat(sp, length);
    label.name_len = length;

    capture_array.push({});
    label.cap = capture_array.virtualAddr(capture_array.len() - 1);

    label_array.push(label);

    sp = regexp + length;
    trackLeave();
    return length;
}
