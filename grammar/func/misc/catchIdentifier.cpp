//
// Created by liu on 23-5-20.
//


#include "Interpreter.h"

xLong xLex::Interpreter::catchIdentifier(char_t *regexp) {
    char_t *sp = regexp;
    trackEnter();

    if (('a' <= *sp && *sp <= 'z')
        || ('A' <= *sp && *sp <= 'Z')
        || (*sp == '_')) {
        sp++;
    } else {
        trackLeave();
        return 0;
    }
    while (true) {
        if (('a' <= *sp && *sp <= 'z')
            || ('A' <= *sp && *sp <= 'Z')
            || ('0' <= *sp && *sp <= '9')
            || (*sp == '_')) {
            sp++;
        } else break;
    }

    trackLeave();
    return sp - regexp;
}
