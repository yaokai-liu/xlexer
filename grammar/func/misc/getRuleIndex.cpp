//
// Created by liu on 23-5-21.
//

#include "xLex_String.h"
#include "Interpreter.h"
using namespace xre;

xLong xLex::Interpreter::getRuleIndex(char_t * src, xLong length) {
    for (xuInt i = 0; i < rule_array.len(); i++) {
        xBool _ = (length == rule_array[i].name_len);
        auto * name = plain_array.realAddr(rule_array[i].name);
        _ = _ && strcmp_i(src, name, length);
        if (_) return i;
    }
    return -1;
}