//
// Created by liu on 23-5-20.
//
#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::parseClass(char_t *regexp) {
    thread_local static xVoid *p_a_array[] = {&plain_array, &inv_plain_array};
    thread_local static xVoid *r_a_array[] = {&range_array, &inv_range_array};
    thread_local static xBool inverse = false;

    char_t *sp = regexp;
    trackEnter();

    eat_keyword(ParseExpStruct::begin_class, 1);

    auto *p = (typeof(plain_array) *) p_a_array[inverse];
    auto *r = (typeof(range_array) *) r_a_array[inverse];
    while (*sp && *sp != ParseExpStruct::end_class) {
        switch (*sp) {
            case ParseExpStruct::inverse: {
                inverse = !inverse;
                sp++;
                continue;
            }
            case ParseExpStruct::range_to: {
                sp++;
                if (stridx(ParseExpStruct::NON_PLAIN, *sp) > 0) {
                    err_pos = sp;
                    err_len = 1;
                    return 0;
                }
                char_t s = {};
                (p)->pop(&s);
                ParseExpStruct::Range range = {s, *sp};
                r->push(range);
                sp++;
                break;
            }
            case ParseExpStruct::escape: {
                sp++;
                xInt index = stridx(ParseExpStruct::CLASS_ESCAPE, *sp);
                if (index >= 0) {
                    // {idx} determinate witch TEMP uses, and
                    // {inv} determinate witch kind of arrays receives.
                    //
                    // For {inv}, if {index % 2 == false}, means not inverse from {CLASS_TEMP},
                    // then if {inverse == false}, using the {plain_array} and {range_array},
                    // or if {inverse = true}, using {inv_*_array}s;
                    // else means inverse from {CLASS_TEMP},
                    // even if {inverse == false}, using still {inv_*_array}s,
                    // until {inverse == true}, using {*_array}s.
                    // Sum up, when {index % 2 != inverse}, using {inv_*_array}s,
                    // otherwise using {*_array}s.
                    xuInt idx = index / 2, inv = ((index % 2) != inverse);
                    auto *cls = &ParseExpStruct::CLASS_TEMP[idx];
                    auto *tp = (typeof(plain_array) *) p_a_array[inv];
                    auto *tr = (typeof(range_array) *) r_a_array[inv];
                    tp->concat(cls->plains, cls->n_plains);
                    tr->concat(cls->ranges, cls->n_ranges);
                    sp++;
                } else {
                    sp += parseSingleEscape(sp);
                    if (err_len > 0) return 0;
                }
                break;
            }
            case ParseExpStruct::begin_class: {
                sp += parseClass(sp);
                if (err_len > 0) return 0;
                break;
            }
            default: {
                p->push(*sp);
                sp++;
            }
        }
    }

    eat_keyword(ParseExpStruct::end_class, 1);

    trackLeave();
    return sp - regexp;
}
