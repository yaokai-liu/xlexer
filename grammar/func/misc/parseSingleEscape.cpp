//
// Created by liu on 23-5-20.
//
#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::parseSingleEscape(char_t *regexp) {
    char_t *sp = regexp;
    trackEnter();

    eat_keyword(ParseExpStruct::escape, 1);

    xInt index = stridx(ParseExpStruct::SINGLE_ESCAPE, *sp);
    if (index >= 0) {
        // Since NON_PLAIN is corresponded with SINGLE_ESCAPE,
        // it will use NON_PLAIN to save space of jump table here.
        plain_array.push(ParseExpStruct::NON_PLAIN[index]);
        sp++;
        trackLeave();
        return sp - regexp;
    }
    switch (*sp) {
        case 'u': {
            // TODO: parse unicode
        }
        case 'x': {
            // TODO: parse unsigned hexadecimal integer <max 64 bit>
        }
        default: {
            err_pos = sp;
            err_len = 1;
            return 0;
        }
    }
}
