//
// Created by liu on 23-5-20.
//

#include "Interpreter.h"
#include "xLex_String.h"

using namespace xLex;
using namespace xre;

[[maybe_unused]]
xInt Interpreter::testEscape(char_t *regexp) {
    char_t *sp = regexp;
    trackEnter();
    if (*sp != ParseExpStruct::escape)
        return ParseExpStruct::non_escape;
    sp++;

    if (stridx(ParseExpStruct::CLASS_ESCAPE, *sp) >= 0) {
        return ParseExpStruct::class_escape;
    }
    return ParseExpStruct::plain_escape;
}
