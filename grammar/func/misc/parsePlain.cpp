//
// Created by liu on 23-5-20.
//

#include "Interpreter.h"
#include "xLex_String.h"

xLong xLex::Interpreter::parsePlains(char_t *regexp) {
    char_t *sp = regexp;
    trackEnter();
#define CHECK_NON_PLAIN stridx(ParseExpStruct::NON_PLAIN, *sp)
    while (*sp && CHECK_NON_PLAIN < 0) sp++;
#undef CHECK_NON_PLAIN
    if (sp > regexp) {
        plain_array.concat(regexp, sp - regexp);
    }
    trackLeave();
    return sp - regexp;
}

