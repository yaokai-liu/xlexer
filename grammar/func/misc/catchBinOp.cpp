//
// Created by liu on 23-5-20.
//

#include "xLex_String.h"
#include "xLex_builtin.h"
#include "Interpreter.h"

xLong xLex::Interpreter::catchBinOp(char_t *src, builtin::functor *val_dest) {
    char_t *sp = src;
    trackEnter();

    switch (sp[0]) {
        case ArithStruct::plus: {
            *val_dest = builtin::i_add_i;
            sp ++; break;
        }
        case ArithStruct::minus: {
            *val_dest = builtin::i_sub_i;
            sp ++; break;
        }
        case ArithStruct::times: {
            *val_dest = builtin::i_mul_i;
            sp ++; break;
        }
        case ArithStruct::div: {
            *val_dest = builtin::i_div_i;
            sp ++; break;
        }
        case ArithStruct::mod: {
            *val_dest = builtin::i_mod_i;
            sp ++; break;
        }
        case ArithStruct::gt: {
            *val_dest = builtin::i_gt_i;
            sp ++; break;
        }
        case ArithStruct::lt: {
            *val_dest = builtin::i_lt_i;
            sp ++; break;
        }
        case ArithStruct::equiv: {
            *val_dest = builtin::i_eq_i;
            sp ++; break;
        }
        case ArithStruct::l_and: {
            *val_dest = builtin::b_and_b;
            sp ++; break;
        }
        case ArithStruct::l_or: {
            *val_dest = builtin::b_or_b;
            sp ++; break;
        }
        default:
            *val_dest = builtin::invalid;
            trackLeave();
            return 0;
    }

    sp = src + 1;
    trackLeave();
    return sp - src;
}