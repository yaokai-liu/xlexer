//
// Created by liu on 23-7-10.
//

#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::getTokenDefinitionIndex(char_t * src, xLong length) {
    for (xuInt i = 0; i < token_definition_array.len(); i++) {
        xBool _ = token_definition_array[i].name_len == length;
        auto * name = plain_array.realAddr(token_definition_array[i].name);
        _ = _ && strcmp_i(src, name, length);
        if (_) return i;
    }
    return -1;
}