//
// Created by liu on 23-5-20.
//
#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::parseValLiteral(char_t *regexp, xLong *index, xBool *is_group) {
    char_t *sp = regexp;
    trackEnter();

    xLong length;
    *index = 0;
    if ('0' <= *sp && *sp <= '9') {
        length = str2l_ud(sp, index);
        *is_group = true;
        sp = regexp + length;
        trackLeave();
        return length;
    }

    length = catchIdentifier(sp);
    check(length > 0, 1);
    check(getLabelIndex(sp, length) != -1, length);
    *is_group = false;

    sp = regexp + length;
    trackLeave();
    return length;
}
