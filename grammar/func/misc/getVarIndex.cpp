//
// Created by liu on 23-5-21.
//


#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::getVarIndex(char_t * src, xLong length) {
    for (xuInt i = 0; i < program_var_array.len(); i++) {
        xBool _ = length == program_var_array[i].name_len;
        auto * name = plain_array.realAddr(program_var_array[i].name);
        _ = _ && strcmp_i(src, name, length);
        if (_) return i;
    }
    return -1;
}
