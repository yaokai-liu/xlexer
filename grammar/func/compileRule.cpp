//
// Created by liu on 23-5-21.
//

#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compileRule(char_t *src) {
    Rule rule = {};
    cur_rule = src;
    char_t *sp = src;
    trackEnter();

    // get token name of the rule
    xLong length = catchIdentifier(sp);
    check(length > 0, 1);
    xLong token_idx = getTokenDefinitionIndex(sp, length);
    check( token_idx >= 0, length);
    sp += length;

    sp += passWhitespace(sp);

    // get rule name
    length = catchIdentifier(sp);
    check(length > 0, 1);                            // There must be an identifier as rule's name,
    check(getRuleIndex(sp, length) < 0, length); // but it should not in rule array yet.
    char_t * tp = sp; // anchor the rule name.
    sp += length;

    sp += passWhitespace(sp);

    xLong grp_index{};
    sp += compileGroup(sp, &grp_index);
    if (err_len > 0) return 0;

    sp += passWhitespace(sp);

    sp += compileProgramBlock(sp);
    if (err_len > 0) return 0;

    rule.name = plain_array.virtualAddr(plain_array.len());
    plain_array.concat(tp, length);
    rule.name_len = length;
    rule.token_type = token_definition_array.virtualAddr(token_idx);
    rule.parser = group_array.virtualAddr(grp_index);
    rule.program = program_block_array.virtualAddr(program_block_array.len() - 1);
    rule_array.push(rule);

    trackLeave();
    return sp - src;
}
