//
// Created by liu on 23-7-22.
//

#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compile(char_t *src) {
    err_pos = nullptr; err_len = 0; cur_text = src;
    cur_line_start = cur_line_start ? cur_line_start : src;
    char_t *sp = src;
    trackEnter();

    while (*sp && err_len == 0) {
        sp += passWhitespace(sp);
        // get token name of the rule
        xLong length = catchIdentifier(sp);
        if (strcmp_i(xLex::DefinitionStruct::KW_TOKEN, sp, length)) {
            sp += compileTokenDefinition(sp);
            if (err_len > 0) return 0;
        } else {
            sp += compileRule(sp);
            if (err_len > 0) return 0;
        }
    }

    trackLeave();
    return sp - src;
}
