//
// Created by liu on 23-5-20.
//

#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compileStatement(char_t *src, xBool * is_final) {
    char_t *sp = src;
    trackEnter();
    xLong length;

    // if assignment
    length = testAssignment(src);
    if (length > 0) {
        length = compileAssignment(sp);
        if (err_len > 0) return 0;
        sp += length;
        trackLeave();
        return sp - src;
    }

    // if final
    if (*sp == ArithStruct::meta::last_val) {
        *is_final = true;
        return compileFinalStatement(sp);
    }

    // otherwise control statement
    return compileControlStatement(sp);
}
