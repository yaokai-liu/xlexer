//
// Created by liu on 23-6-12.
//

#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compileFinalStatement(char_t *src) {
    Stack<ArithStruct::ArithExpression> member_array{allocator};
    ArithStruct::ArithExpression exp{};

    char_t *sp = src;
    trackEnter();

    eat_keyword(ArithStruct::meta::last_val, 1);
    sp += passWhitespace(sp);

    eat_keyword(ArithStruct::MULTI_WORD[ArithStruct::meta::assign], 1);
    sp += passWhitespace(sp);

    xLong member_count = 0;
    eat_keyword(ProgramBlockStruct::begin_block, 1);
    sp += passWhitespace(sp);
    if (*sp == ProgramBlockStruct::begin_block) {
        sp++; // eat begin_block
        trackLeave();
        return sp - src;
    }

    member_array.init(); // using a temporary array to store members
    __parse_members:
    sp += compileArithExpression(sp);
    if (err_len > 0) return 0;
    expr_array.pop(&exp);
    member_array.push(exp);
    sp += passWhitespace(sp);

    if (*sp == ArithStruct::comma) {
        sp++; // eat comma
        goto __parse_members;
    }

    eat_keyword(ProgramBlockStruct::end_block, 1);


    // TODO: compose struct of members
    member_count = member_array.len();
    member_array.clear();

    trackLeave();
    return sp - src;
}
