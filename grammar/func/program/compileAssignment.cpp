//
// Created by liu on 23-5-20.
//

#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compileAssignment(char_t *src) {
    char_t *sp = src;
    trackEnter();

    sp += passWhitespace(sp);
    char_t *var_name = sp;
    xLong var_name_len = catchIdentifier(sp);
    if (var_name_len == 0) return 0;
    check(getVarIndex(var_name, var_name_len) < 0, var_name_len);
    sp += var_name_len;
    sp += passWhitespace(sp);

    eat_keyword(xReChar('='), 1);

    sp += passWhitespace(sp);
    sp += compileArithExpression(sp);
    if (err_len > 0) return 0;

    eat_keyword(ArithStruct::semicolon, 1);

    ProgramBlockStruct::ProgramVariable var = {};
    var.name = plain_array.virtualAddr(plain_array.len());
    plain_array.concat(var_name, var_name_len);
    var.name_len = var_name_len;
    var.val = expr_array.virtualAddr(expr_array.len() - 1);
    var.type = expr_array.realAddr(var.val)->d_type;
    program_var_array.push(var);


    trackLeave();
    return sp - src;
}
