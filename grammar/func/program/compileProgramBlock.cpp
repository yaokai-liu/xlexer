//
// Created by liu on 23-5-20.
//

#include "Interpreter.h"

xLong xLex::Interpreter::compileProgramBlock(char_t *src) {
    char_t *sp = src;
    trackEnter();

    check(sp[0] == ProgramBlockStruct::begin_block, 1);
    sp++;

    sp += passWhitespace(sp);

    xBool is_final = false;
    while (sp[0] && sp[0] != ProgramBlockStruct::end_block && !is_final) {
        sp += compileStatement(sp, &is_final);
        if (err_len > 0) return 0;
        sp += passWhitespace(sp);
    }

    check(sp[0] == ProgramBlockStruct::end_block, 1);
    sp++;

    trackLeave();
    return sp - src;
}
