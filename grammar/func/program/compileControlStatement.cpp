//
// Created by liu on 23-6-9.
//
#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::compileControlStatement(char_t *src) {
    char_t *sp = src;
    trackEnter();
    sp += passWhitespace(sp);
    ProgramBlockStruct::ControlStatement ctrl = {};

    sp += compileSingleExpr(sp);
    if (err_len > 0) return 0;
    ctrl.cond_expr = expr_array.virtualAddr(expr_array.len());

    sp += passWhitespace(sp);
    eat_keyword(ArithStruct::ques_mark, 1);
    sp += passWhitespace(sp);

    if (*sp == ProgramBlockStruct::begin_block) {
        ctrl.type = ProgramBlockStruct::ControlStatement::blk;
        sp += compileProgramBlock(sp);
        if (err_len > 0) return 0;
        xLong index = program_block_array.len();
        ctrl.loop_body.block = program_block_array.virtualAddr(index);
        sp += passWhitespace(sp);
        if (*sp == ArithStruct::colon) {
            sp ++;
            sp += passWhitespace(sp);
            sp += compileProgramBlock(sp);
            if (err_len > 0) return 0;
            index = program_block_array.len();
            ctrl.else_part.block = program_block_array.virtualAddr(index);
        } else {
            ctrl.loop_body.block = nullptr;
        }
    } else {
        ctrl.type = ProgramBlockStruct::ControlStatement::exp;
        sp += compileArithExpression(sp);
        if (err_len > 0) return 0;
        xLong index = expr_array.len() - 1;
        ctrl.loop_body.expr = expr_array.virtualAddr(index);
        sp += passWhitespace(sp);
        if (*sp == ArithStruct::colon) {
            sp ++;
            sp += passWhitespace(sp);
            sp += compileArithExpression(sp);
            if (err_len > 0) return 0;
            index = expr_array.len() - 1;
            ctrl.else_part.expr = expr_array.virtualAddr(index);
        } else {
            ctrl.loop_body.expr = nullptr;
        }
        eat_keyword(ArithStruct::semicolon, 1);
    }

    sp += passWhitespace(sp);

    trackEnter();
    return sp - src;
}