//
// Created by liu on 23-5-20.
//

#include "xLex_String.h"
#include "Interpreter.h"

xLong xLex::Interpreter::testAssignment(char_t *src) {
    char_t *sp = src;
    trackEnter();

    xLong length = catchIdentifier(sp);
    if (length == 0) return 0;
    sp += length;
    sp += passWhitespace(sp);
    auto * expected = ArithStruct::MULTI_WORD[ArithStruct::meta::assign];
    if (1 == strcmp_o(sp, expected)) {
        trackLeave();
        return length;
    }
    return 0;
}
