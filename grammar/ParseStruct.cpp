//
// Created by liu on 23-5-20.
//
#include "ParseStruct.h"

namespace xLex::ParseExpStruct {
    // const data, read only
#define CTL_SYM "()[]{}<>\\|@#$&~:;=+-*/?!^."
#define BLK_SYM "\n\r\v\f\t "
    // NON_PLAIN must be corresponded with SINGLE_ESCAPE
    const char_t * const NON_PLAIN = xReString(BLK_SYM CTL_SYM "\0");
    const char_t * const SINGLE_ESCAPE = xReString("nrtfv " CTL_SYM "\0");
    const char_t * const CLASS_ESCAPE = xReString("sSwWdDcC" "\0");
    const Class CLASS_TEMP[4] = {
            // \s or \S
            {.n_plains = 6, .plains = (char_t *) xReString(BLK_SYM)},
            // \w or \W
            {.n_plains = 1, .n_ranges = 3,
                    .plains = (char_t *) xReString("_"), .ranges = (Range *) xReString("azAZ09")},
            // \d or \D
            {.n_ranges = 1, .ranges = (Range *) xReString("09")},
            // \c or \C, must be same with CTL_SYM
            {.n_plains = 26, .plains = (char_t *) CTL_SYM}
    };
#undef CTL_SYM
#undef BLK_SYM
}
