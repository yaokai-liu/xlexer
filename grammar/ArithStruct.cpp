//
// Created by liu on 23-5-20.
//

#include "ArithStruct.h"

using namespace xre;

namespace xLex::ArithStruct {
    const char_t * MULTI_WORD[] = {
        [0] = nullptr,
        [assign] = xReString("="),
        [equiv] = xReString("=="),
        [ge] = xReString(">="),
        [le] = xReString("<="),
    };
}
