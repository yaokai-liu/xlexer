//
// Created by Yaokai Liu on 2023/4/13.
//

#include "Interpreter.h"
#include "xLex_String.h"

namespace xLex {
    [[maybe_unused]] Interpreter::Interpreter(Allocator *allocator) : allocator(allocator) {
        plain_array.init();
        range_array.init();
        inv_plain_array.init();
        inv_range_array.init();
        expr_array.init();
        seq_array.init();
        class_array.init();
        quantifier_array.init();
        branch_array.init();
        group_array.init();
        capture_array.init();
        label_array.init();
        refer_array.init();
        obj_array.init();
        program_var_array.init();
        program_stat_array.init();
        program_block_array.init();
        rule_array.init();
#ifdef XLEXER_DEBUG
        trace_indent.init();
#endif

        staticDataInit();
    }

    Interpreter::~Interpreter() {
        plain_array.clear();
        range_array.clear();
        inv_plain_array.clear();
        inv_range_array.clear();
        expr_array.clear();
        seq_array.clear();
        class_array.clear();
        quantifier_array.clear();
        branch_array.clear();
        group_array.clear();
        capture_array.clear();
        label_array.clear();
        refer_array.clear();
        obj_array.clear();
        program_var_array.clear();
        program_stat_array.clear();
        program_block_array.clear();
        rule_array.clear();

#ifdef XLEXER_DEBUG
        trace_indent.clear();
#endif
    }

    // you are not expected to understand this
    xLong Interpreter::logError(char_t *log_dest) {

        auto *ERR_LOG_0 = xReString("Failed parsing text at line % colon %:\n\t" "\0");
        if (err_len == 0) return 0;

        char_t *sp = log_dest;
        xLong err_coord[2] = {};
        posToRowCol(&err_coord[0], &err_coord[1]);
        for (xInt i = 0, j = 0; ERR_LOG_0[i]; i++) {
            if (ERR_LOG_0[i] == xReChar('%')) {
                sp += l2str_d(err_coord[j], sp);
                j++;
                continue;
            }
            *sp = ERR_LOG_0[i];
            sp++;
        }
#ifndef ERR_TERM_LEN
#define ERR_TERM_LEN 100
#endif
        char_t *cur = (err_coord[1] < ERR_TERM_LEN)
                       ? cur_line_start
                       : err_pos - ERR_TERM_LEN;
#define break_cond (cur[i] && i < 2 * ERR_TERM_LEN && stridx(xReString("\n\v"), cur[i]) < 0)
        for (xInt i = 0; break_cond; i++, sp++) {
            *sp = cur[i];
        }
#undef break_cond
        sp[0] = xReChar('\n'), sp[1] = xReChar('\t');
        sp += 2;
        for (xInt i = 0; i < err_pos - cur; i++, sp++) {
            sp[0] = xReChar(' ');
        }
        for (xInt i = 0; i < err_len; i++, sp++) {
            sp[0] = xReChar('^');
        }
        sp[0] = xReChar('\n');
        sp++;

        allocator->memcpy(sp, (void *) err_msg, msg_len);
        sp += msg_len;
        sp[0] = xReChar('\n');
        sp++;

        return sp - log_dest;
    }

    xVoid Interpreter::posToRowCol(xLong *err_row, xLong *err_col) {
        *err_row = cur_row + 1;
        *err_col = err_pos - cur_line_start + 1;
    }

    xBool Interpreter::hasError() const {
        return err_len > 0;
    }

    xVoid Interpreter::staticDataInit() {
        auto _ = const_cast<ArithStruct::ArithExpression *>(ArithStruct::BUILTIN_EXPRS);
        expr_array.concat(_, 2);
    }
}


const static xre::char_t error_message[50] = {
    // TODO: error messages
};