//
// Created by Yaokai Liu on 2023/5/7.
//

#include "Interpreter.h"
#include <cstdlib>
#include <cstring>
#include "xReChar.h"
#include <cstdio>

int main(int argc, char *argv[]) {
    xLex::Allocator allocator = {.malloc = malloc, .realloc = realloc,
            .calloc = reinterpret_cast<void *(*)(xLong, xSize)>(calloc), .free = free,
            .memcpy = reinterpret_cast<void (*)(xVoid *, xVoid *, xSize)>(memcpy),
            .memset = reinterpret_cast<void (*)(xVoid *, xuByte, xSize)>(memset)};
    xLex::Interpreter interpreter = xLex::Interpreter(&allocator);

    xre::char_t *text1 = xReString("token {\n"
                                   "\tint32 a;\n"
                                   "\tstruct {\n"
                                   "\t\tint32 b;\n"
                                   "\t} s;\n"
                                   "} token1;\n\n"
                                   "token1 text1 (asd [abc]{1} | (sij*) 001002=<num> $<num> \\s) "
                                   "{\n"
                                   "\tvar1 = 10 + num; \n"
                                   "\tvar2 = 10 + #1; \n"
                                   "\tvar3 = $1; \n"
                                   "\tvar4 = $num; \n"
                                   "}");
    printf("Parsing text: \n\n%s\n\n", text1);

    interpreter.compile(text1);
    xre::char_t log[512] = {};
    if (interpreter.hasError()) {
        interpreter.logError(log);
        printf("Error occurs: %s\n", log);
    }
    return 0;
}
