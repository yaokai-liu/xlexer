cmake_minimum_required(VERSION 3.24)
project(xLex CXX)

set(CMAKE_CXX_STANDARD 23)
#set(CMAKE_BUILD_TYPE Release)
#add_compile_options("-O3")

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/output)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/output)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/output)

add_compile_definitions("XTYPES_USING_STDC_TYPE")
add_compile_definitions("XDEF_USING_C_STD_DEF")
add_compile_definitions("XRE_CHAR_USING_ASCII")
add_compile_definitions("XLEXER_DEBUG")

include_directories(xdef)
include_directories(include)

aux_source_directory(builtin BUILTIN)
file(GLOB_RECURSE GRAMMAR grammar/*.cpp)
aux_source_directory(sidelibs SIDELIBS)
add_library(xLex-Interpreter ${BUILTIN} ${GRAMMAR} ${SIDELIBS})
target_include_directories(xLex-Interpreter PRIVATE internal_headers)

add_subdirectory(test)
