
# xLex Engine

xLex engine is a regular expression based text parsing and processing program.

xLex Engine reads xLex rules and uses them to process the input text and output the result.

## Grammar

The regular expression xLex using is a special regular expression called `xLex Regexp`.

And xLex using a special programming language to describe how to compose output text, too, called `xLex Program`.

Every xLex rule should be led with an e_type of token the rule returns and an IDENTIFIER that rule name.
Then xLex regexp follows it in a parentheses, and a xLex program follows.

It is in grammar expression:
```
xLex_Rules ::= ("void" | TOKEN) IDENTIFIER "(" xLex_Regexp ")" xLex_Program

```

**xLex Regexp**

Some grammars of xLex regexp's defined as follows:
```

xLex_Regexp ::= ( "~" )? xLex_Regexp ( "&" )?

xLex_Regexp ::= Branch "|" xLex_Regexp
             | Branch


Branch ::= (xLex_Object)+


xLex_Object ::= Group | Sequence | Character_Class 
             | Quantifiered | Assigned | Called | Switch
             | ATTR Xre_Object


Group ::= "(" xLex_regexp ")"

Sequence ::= ( PLAIN_CHAR | SINGLE_ESCAPE )+

Character_Class ::= "[" (PLAIN_CHAR | SINGLE_ESCAPE | Range | Character_Class | Class_Macro)+ "]"
                  | CLASS_ESCAPE

Quantifiered ::= xLex_Object Quantifier

Assigned ::= xLex_Object "=" Label

Called ::= ("@" | "$") Label

Switch ::= "<" ArithExpression "?" xLex_Regexp ">"



Range ::= PLAIN_CHAR "-" PLAIN_CHAR

Class_Macro ::= ":" IDENTIFIER ":"

Quantifier ::= "{" ArithExpression? ","? ArithExpression? ";"? ArithExpression? "}"
             | QUANTIFIER_MACRO

Label ::= "<" Literal ">"

```

Unlike those regular expression, xLex regexp is designed for not only parsing but also capturing.
So connected plain characters will be combined as an integration, i.e. `Sequence`.

Also, xLex regexps use blank characters as separators for `ReObj`s, this is not in other regular expression.

**xLex Program**

And some grammars of xLex program's defined as follows:
```
xLex_Program ::= "{" ProgramStatement* "}"

ProgramStatement ::= Assignment 
                   | ControlStatement 
                   | ReturnStatement


Assignment ::= IDENTIFIER ":=" ArithExpression ";"

SplitStatement ::= Parenthesis "?" Options ";"
 
ControlStatement ::= Parenthesis "?" LoopBody

ReturnStatement ::= "$" ":=" ( ArithExpression | "(" Args ")" )

ArithExpression ::= SingleExpr BIN_OP ArithExpression
             | SingleExpr 

Options ::= ArithExpression (":" ArithExpression)*

LoopBody ::= "{" ProgramStatement* "}"

SingleExpr ::= FUNC_NAME "(" Arg? ")"
             | SIN_OP SingleExpr
             | Parenthesis
             | Literal

Args ::= ArithExpression ("," ArithExpression)*

Parenthesis ::= "(" ArithExpression ")"

Literal ::= IDENTIFIER | NUMBER | ("#" NUMBER) | Capture

Capture ::= "$" (IDENTIFIER | NUMBER )
```

Unlike those programming languages, the syntax of xLex is only designed for text processing,
and in simplicity.

**Token Type**

xLex need some way to describe what is the memory structure of a rule's result.
xLex uses a `struct` in C like grammar to deal with it, called `xLex Token`.
A xLex token will be defined with a structure, that led with a keyword `token`, and bracketed with `{` and `}`.

Like struct in C, xLex need types to determinate size of members of tokens. But not all types are legal in xLex.
xLex only support `int16`, `int32`, `int64`, `char`, `string` and `bool`,
which means 16-bit integer, 32-bit integer, 64-bit integer, 8-bit data, address of a string, and the bool e_type(1 byte).
But tokens are not types.

For a better using, xlex provides `struct` and `union` to describe continuous memories and members' shared memories.
These two structures can be nested.

Besides, `void` is also can be used as a type, to describe those rules that process ignored input.

With those types and `void`, the grammar to define a xLex token will be like:
```
TokenDef ::= "token" "{" (BUILTIN_TYPE MEMBER_NAME ";")* "}"  IDENTIFIER ";"

```

Note that xLex allows an empty token as result of a rule. And xLex disallows a nesting token in a token.


## xLex Regexp Structures

### Escape

**Definition:**
An escape is `\ ` concat with some characters.
escape has two kinds, `SINGLE_ESCAPE` and `CLASS_ESCAPE`.
`SINGLE_ESCAPE` will be viewed as a plain character and integrated into a `Sequence`.
`CLASS_ESCAPE` will be viewed as a character class.

All escapes please refer the [escape table](#escape-table).

### Plain Text (Sequence)

**Definition:** 
Plain text(`Sequence`) is strings with only `PLAIN_CHAR` or `SINGLE_ESCAPE`. 

**Breaking:** 
And blank characters like ` `(space), `\n`(line feed), `\t`(horizontal tab), etc.,
i.e. all characters that `\s` in other kinds of normal regular expression, 
will be viewed as breaking of plain text.

**Function:**
Match a sequence is to match every character once in order, even exists duplicates.

### Character Class

**Definition**:
A `Character_Class` is characters bracketed with `[` and `]`. 

**Function:**
Match a set is to match any none `-` character in the set's characters once, but include characters between
the two characters concat with `-`.

1. **Range:** a range is a continuous character set, that described with a `-` in its start character and end character.
2. **Class Macro:** a class macro is a class that builtin with xLex processor. *(not implemented yet)*

### Group

**Definition:**
A `Group` is an xLex regexp bracketed with `(` and `)`. 

**Function:**
Match a group is match the xLex regexp in it.

**Feature 1:**
When creating a group, there will create a label refer to it, 
and the label's name always is a number, according to the `(`'s orders.

### Branch

**Definition:**
`Branch`s are those xLex regexps split by `|`, and every part should not include a `|` stands.
Those parts are called `Branch`.

**Feature:**
Branches in a group share group's inner common environment.

### Quantifier

**Definition:**
A `Quantifier` is some expressions bracketed with `{` and `}` but divided with `,` and `;`.
Number of expressions is 0 to 3, while the `,` and `;` can be omitted.

The first expression is the lower bound(included),
and the second is the upper bound(included, too).
The third expression is the step count of match. 

Value of lower bound and upper bound must be non-negative integer, and step's must be positive integer.

But if the step is set as a non-positive, the parser will be performed with a non-defined behavior.

Upper bound's allowed reach the positive infinity. 

And:
1. If lower bound is not set, it will be set to 0 by default;

2. if upper bound is not set, it will default to positive infinity;

3. if step count is not set, it will be set as 1 by default.

### Quantifier Macros

xLex macros are those strings to replace with their plains in an xLex regexp before match stage.

All xLex macros are:

| Macros | Value                 |
|:-------|:----------------------|
| `*`    | `{,;}` or `{0,;1}`    |
| `+`    | `{1,;}` or `{1,;1}`   |
| `?`    | `{0,1;}` or `{0,1;1}` |


**Function:**
Quantifier will not do any match operation. But it will let the executor repeat the last struct before it at least its
lower bound and at most its upper bound, for every step count.

### Switch 
*(not implemented yet)*

**Definition:**
A `Switch` is an expression and an xLex regexp bracketed with `<` and `>` but seperated with `?` in order.

**Function:**
If the value of the expression is not a zero, the executor will execute the xLex.
If the xLex regexp has more than one branches, xLex parser will execute the branch the expression referred.

### Label

**Definition:**
A `Label` is a number or an identifier bracketed with `<` and `>`.

`Label`s can be created by `=` after a `ReObj`, also can be used led by `@` or `$`.

When a label is creating, it will refer to the previous `ReObj`, 
and create a capture to store its last matching result.

**Feature:**
When using `@` to call a label, executor will execute its referring, and update its capture;
When using `$` to call a label, executor will match its last capture.

**Restrict:**
When creating a label, the label name must be identifier, numbers are forbidden.

### Capture

**Definition:**
A `Capture` is a piece of memory space to store results of executing xLex regexp.

There are only groups and labels can and will create captures.

### xLex Attributes

xLex structures can have some extern attributes, them are parts of xLex grammar. 
xLex attributes tell parser and executor how to process those suffixed structures.

xLex attributes determinate some special behaviors of parsing and matching.
- **Only Parse:**
Character `!` is an attribute prefix. It means the xLex structure string it prefixes will only compileGroup to an xLex structure,
but will do no matching.

- **Inverse:**
Character `^` is an attribute prefix. It is matched only when the structure it prefixes is not matched.

- **Other Special Attributes:**
*(no more yet)*



## xLex Program Structures

An xLex rule has only one xLex program.
xLex programs in xLex rules must be bracketed by `{` and `}`.

xLex program is to process captured results from xLex expression of current xLex rule.

### Program Statement

Statements of xLex program are C like statements, except assignments. 

There is no declaration in xLex program.
Type of Variables are only two kinds, integer or string.
And user don't need to declare e_type when create a variable.

- **Assignment:**
An assignment will create a variable that alive in current xLex program.

- **Control Statement:**
xLex uses control statements instead of branch or loop statements.

- **Return Statement:**
Every xLexer program has at least one return statement. 
And any control flow will be ended with a return statement.
Those statements after return statement in a control flow will be not executed.


### Arithmetic Expression

**Definition:**
An `ArithExpression` is an arithmetic expression in `{` and `}` brackets in a xLexer expression 
or in statements in a xLexer program.

In xLexer programs, expressions always are part of statements.

There are some different expressions:
- **Literal Values:**
Literal values are those numbers or strings with no variables.

- **Variables**: 
variables are those identifiers consistent with only lower letters, digits and underscore, and must start with lower letters.

- **Literals:**
Literal values and variables are all called literals.

- **Functions**: 
functions are builtin defined by compiler or interrupter.
The name of functions must be UPPER letters, digits and underscore, and must start with UPPER letters.
Arguments of functions are some expressions bracketed with `(` and `)` after function name,
and if more than one should be separated with `,`.
Functions will be list in the [Functions Table](#functions-table)

- **Operator Expressions**: 
other expressions are operator expressions. Normally them have at least one operator.
Operators will be list in the [Operators table](#operators-table).

**Note:**
Literals, functions, parentheses, single operator expressions are all called single expressions.

**Note:**
arithmetic expressions of xLexer program all are lazy evaluated, 
because xLexer program is what user tells to xLexer executor only how the result `$` is computed.



## Functions Table


## Escape Table


## Operators Table

